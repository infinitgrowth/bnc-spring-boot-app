


package com.edgenda.bnc.skillsmanager.controllerREST;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// RestControllers rendering resources in different formats to be used by other services
@RestController
// Specify URI for RestAPI call (in postman)
@RequestMapping ("/employees")
public class EmployeeController {


}
